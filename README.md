# Bord de l'eau

## Dev

Generate CSS with Tailwind (the typography module is needed):
```
tailwindcss -i ./main.css -o ./static/css/main.css --watch
```

Generate complete site with Zola ([those](https://github.com/ppom0/zola/commit/404de294ffc3b3e2e15952790c41ade74bb7d935) [patches](https://github.com/ppom0/zola/commit/d23eded6bcd95d475340b3bf40d7d4eb17c028e3) are needed):
```
zola serve --store-html
```
