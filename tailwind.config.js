/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
	//content: ["./templates/*.html", "./templates/**/*.html", "public/*.html", "public/**/*.html"],
	content: ["./**/*.html"],
	theme: {
		extend: {
			fontFamily: {
				'sans': ['Gill Sans', 'Aller', ...defaultTheme.fontFamily.sans],
				'title': ['Phenomena', 'Gill Sans', 'Aller', ...defaultTheme.fontFamily.sans],
			},
			colors: {
				'green-tone-8': '#faf8ec',
				'green-tone-7': '#eef3ec',
				'green-tone-5': '#a9c6a8',
				'green-tone-4': '#6c9e85',
				'green-tone-2': '#2f5141',
				'rose-tone-7': '#f9d5d9',
				'rose-tone-5': '#cc1f89',
				'bordeaux-tone-3': '#a63c52',
				'graylight': '#cccccc',
				'triangle-1': '#c03152',
				'triangle-2': '#28614e',
				'triangle-3': '#f7c2c6',
				'triangle-4': '#e3ea78',
			},
			typography: () => ({
				DEFAULT: {
					css: {
						maxWidth: '100%',
					}
				}
			}),
		},
	},
	plugins: [
		require('@tailwindcss/typography'),
	],
}

